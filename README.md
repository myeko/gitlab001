
# GitLab001
Design a microPi HAT compliant PCB.
This microHAT will be a UPS (uninterrupted power supply) to keep your Pi alive for long enough to shut it
down properly (or if your battery is big enough to survive the blackout).
Pi HAT is going to be connected parallel to the main source so that the battery can be charged during normal operations. The system will have status LEDs to indicate battery life and boost converter to convert the voltage to the appropriate voltage for Pi.In case the battery gets really low the LEDs will only flash red signaling a smooth shutdown to be performed by pi.
